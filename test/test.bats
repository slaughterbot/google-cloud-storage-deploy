#!/usr/bin/env bats

# Test GCP storage, https://cloud.google.com/storage/

set -e

setup() {
  IMAGE_NAME=${DOCKERHUB_IMAGE}:${DOCKERHUB_TAG}
  RANDOM_NUMBER=$RANDOM
  LOCAL_DIR=tmp/gcp-storage-${RANDOM_NUMBER}

  # clean up & create paths
  rm -rf $LOCAL_DIR
  mkdir -p $LOCAL_DIR

  echo "Pipelines is awesome!" > $LOCAL_DIR/deployment-${RANDOM_NUMBER}.txt
}

teardown() {
  # clean up
  rm -rf $LOCAL_DIR
}

@test "upload to GCP storage" {
  # execute tests
  run docker run \
    -e KEY_FILE="${GCP_KEY_FILE}" \
    -e PROJECT="gcp-storage-deploy" \
    -e BUCKET="test-gcp-storage-deploy" \
    -e SOURCE="${LOCAL_DIR}" \
    -e CACHE_CONTROL="max-age: 30" \
    -e CONTENT_DISPOSITION="inline" \
    -e CONTENT_ENCODING="identity" \
    -e CONTENT_TYPE="text/plain" \
    -e ACL="public-read" \
    -e STORAGE_CLASS="nearline" \
    -v $(pwd):$(pwd) \
    -w $(pwd) \
    $IMAGE_NAME

  [[ "${status}" == "0" ]]

  # verify
  run curl --silent "https://storage.googleapis.com/test-gcp-storage-deploy/gcp-storage-${RANDOM_NUMBER}/deployment-${RANDOM_NUMBER}.txt"
  [[ "${status}" == "0" ]]
  [[ "${output}" == "Pipelines is awesome!" ]]
}

@test "fail when source item does not exist" {
  # execute tests
  run docker run \
    -e KEY_FILE="${GCP_KEY_FILE}" \
    -e PROJECT="gcp-storage-deploy" \
    -e BUCKET="test-gcp-storage-deploy" \
    -e SOURCE="does-not-exist" \
    -v $(pwd):$(pwd) \
    -w $(pwd) \
    $IMAGE_NAME

  echo "${output}"
  [[ "${status}" == "1" ]]
  [[ "${output}" =~ "CommandException: 1 file/object could not be transferred." ]]
}


@test "regression multiline key" {

  KEY_ENCODED=`echo ${GCP_KEY_FILE} | base64 -d | base64 -i -`
  # execute tests
  run docker run \
    -e KEY_FILE="${KEY_ENCODED}" \
    -e PROJECT="gcp-storage-deploy" \
    -e BUCKET="test-gcp-storage-deploy" \
    -e SOURCE="${LOCAL_DIR}" \
    -e CACHE_CONTROL="max-age: 30" \
    -e CONTENT_DISPOSITION="inline" \
    -e CONTENT_ENCODING="identity" \
    -e CONTENT_TYPE="text/plain" \
    -e ACL="public-read" \
    -e STORAGE_CLASS="nearline" \
    -v $(pwd):$(pwd) \
    -w $(pwd) \
    $IMAGE_NAME

  [[ "${status}" == "0" ]]

  # verify
  run curl --silent "https://storage.googleapis.com/test-gcp-storage-deploy/gcp-storage-${RANDOM_NUMBER}/deployment-${RANDOM_NUMBER}.txt"
  [[ "${status}" == "0" ]]
  [[ "${output}" == "Pipelines is awesome!" ]]
}

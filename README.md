# Bitbucket Pipelines Pipe: Google Cloud Storage Deploy

Pipe to deploy to [Google Cloud Storage][gsutil-cp].
Copy files, directories and google storage prefixes. Recursively copies new and updated files from the source local directory to the destination. Only creates folders in the destination if they contain one or more files.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/google-cloud-storage-deploy:0.3.6
  variables:
    KEY_FILE: '<string>'
    PROJECT: '<string>'
    BUCKET: '<string>'
    SOURCE: '<string>'
    # CACHE_CONTROL: '<string>' # Optional.
    # CONTENT_DISPOSITION: '<string>' # Optional.
    # CONTENT_ENCODING: '<string>' # Optional.
    # CONTENT_LANGUAGE: '<string>' # Optional.
    # CONTENT_TYPE: '<string>' # Optional.
    # ACL: '<string>' # Optional.
    # STORAGE_CLASS: '<string>' # Optional.
    # EXTRA_ARGS: '<string>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```

## Variables

| Variable                   | Usage                                                |
| ----------------------------- | ---------------------------------------------------- |
| KEY_FILE (*)                  |  base64 encoded Key file for a [Google service account](https://cloud.google.com/iam/docs/creating-managing-service-account-keys). |
| PROJECT (*)                   |  The project that owns the bucket to upload into. |
| BUCKET (*)                    |  Google Cloud Storage bucket name. |
| SOURCE (*)                    |  The source of the files to upload (can be a path to a local file/directory or a Google Cloud Storage URI) |
| CACHE_CONTROL                 |  Caching behavior along the request/reply chain. Valid options are `no-cache`, `no-store`, `max-age=<seconds>`, `s-maxage=<seconds> no-transform`, `public`, `private`. |
| CONTENT_DISPOSITION           |  Indicates if the object is to be displayed inline or or as an attachment. |
| CONTENT_ENCODING              |  Content encodings that have been applied to the object. |
| CONTENT_LANGUAGE              |  The language for the intended audience of the object. |
| CONTENT_TYPE                  |  The media type of the object. |
| ACL                           |  ACL for the object when the command is performed. Valid values are: `project-private`, `private`, `public-read`, `public-read-write`, `authenticated-read`, `bucket-owner-read`, `bucket-owner-full-control`. |
| STORAGE_CLASS                 |  Type of storage to use for the object. Valid options are `multi_regional`, `regional`, `nearline`, `coldline`. |
| EXTRA_ARGS                    |  Extra arguments to be passed to the CLI (see [Google Cloud Storage cp docs][gsutil-cp] for more details). |
| DEBUG                         |  Turn on extra debug information. Default: `false`. |

_(*) = required variable._

More info about parameters and values can be found in the [Google Cloud Storage cp docs][gsutil-cp].

## Examples

### Basic example:

```yaml
script:
  - pipe: atlassian/google-cloud-storage-deploy:0.3.6
    variables:
      KEY_FILE: $KEY_FILE
      PROJECT: 'my-project'
      BUCKET: 'my-bucket'
      SOURCE: '.'
```

### Advanced example: 
    
```yaml
script:
  - pipe: atlassian/google-cloud-storage-deploy:0.3.6
    variables:
      KEY_FILE: $KEY_FILE
      PROJECT: 'my-project'
      BUCKET: 'my-bucket'
      SOURCE: '.'
      CACHE_CONTROL: 'max-age=30'
      CONTENT_DISPOSITION: 'attachment'
      ACL: 'public-read'
      STORAGE_CLASS: 'nearline'
      EXTRA_ARGS: '-e'
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: https://community.atlassian.com/t5/forums/postpage/choose-node/true/interaction-style/qanda?add-tags=bitbucket-pipelines,pipes,google
[gsutil-cp]: https://cloud.google.com/storage/docs/gsutil/commands/cp

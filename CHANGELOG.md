# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.3.6

- patch: Updated contributing guidelines

## 0.3.5

- patch: Prevent pipe from hanging on error.

## 0.3.4

- patch: Standardising README and pipes.yml.

## 0.3.3

- patch: Added support for multiline keys files

## 0.3.2

- patch: Fixed support multi threaded directory uploads.

## 0.3.1

- patch: Fix typo in markdown link reference.

## 0.3.0

- minor: Add support for the DEBUG variable.
- minor: Switch naming conventions from task to pipes.

## 0.2.2

- patch: Use quotes for all pipes examples in README.md.

## 0.2.1

- patch: Restructure README.md to match user flow.

## 0.2.0

- minor: Modified gcloud commands to always use --quiet, to allow fast erroring and use of default settings.

## 0.1.0

- minor: Initial release of Bitbucket Pipelines GCP storage deploy pipe.

